<?php
use think\Cookie;
use think\Db;

//验证是否登陆
function checkSign()
{
    if (Cookie::has('stu_id')) {
        return;
    } else {
        return redirect('/GQT/public/index/index/sign',200);
    }

}

function getCookie()
{
    return Cookie::get('stu_id');
}

function getUserName()
{
    $user_id = Db::table('user')->field(['name'])->where('stu_id', getCookie())->select();
    return $user_id[0]['name'];
}
