<?php
namespace app\index\controller;

use think\Controller;
use think\Cookie;
use think\Db;

class Sign extends Controller
{

    public function sign()
    {
        return $this->fetch('sign');
    }

    public function verifyId($user)
    {
        $res = Db::table('user')->where('stu_id', $user)->find();
        if (is_null($res)) {
            $arr = array(
                'succ' => false,
                'state' => 0,
                'data' => '',
            );
        } else {
            $arr = array(
                'succ' => true,
                'state' => 1,
                'data' => '',
            );
        }
        $res = json_encode($arr);
        return $res;
    }

    public function loginIndex()
    {
        return $this->fetch('login');
    }

    public function submit($user, $password)
    {

        $res = Db::table('user')->where('stu_id', $user)->value('password');
        if ($res == $password) {
            $arr = array(
                'succ' => true,
                'state' => 1,
                'data' => '',
            );
            Cookie::set('stu_id', $user, time() + 3600);
            //           Cookie::set('password',$password,time()+3600);
        } else {
            $arr = array(
                'succ' => false,
                'state' => 0,
                'data' => '',
            );
        }
        $res = json_encode($arr);
        return $res;
    }

    public function login()
    {
        $stu_id = input('post.stu_id'); //$_POST['stu_id'];
        $name = input('post.name'); //$_POST['name'];
        $password = input('post.password'); //$_POST['password'];
        $sex = input('post.sex'); //$_POST['sex'];
        $class = input('post.class');
        $data = ['stu_id' => $stu_id, 'name' => $name, 'password' => $password, 'sex' => $sex, 'power' => '0', 'class' => $class];
        $res = Db::table('user')->insert($data);
        return $res;
    }

    public function logout()
    {
        Cookie::delete('stu_id');
    }

}
