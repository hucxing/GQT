<?php
namespace app\index\controller;

use think\Db;
use think\Validate;

class Apply extends CommonController
{

    //显示申请页面
    public function apply()
    {
        return $this->fetch('apply');
    }

    //申请学分
    public function daclare($type, $name, $stu_id, $stu_name, $tea_id, $tea_name, $content, $oneClass, $twoClass)
    {
        $data = array(
            "type" => $type,
            "name" => $name,
            "stu_id" => $stu_id,
            "stu_name" => $stu_name,
            "tea_id" => $tea_id,
            "tea_name" => $tea_name,
            "content" => $content,
            "oneClass" => $oneClass,
            "twoClass" => $twoClass,
        );

        $rule = ["name" => "require|max:25", //奖项名称
            "stu_id" => "require|cherkStuId", //学生id
            "stu_name" => "require", //学生名字
            "tea_id" => "require|number", //指导教师名字
            "tea_name" => "require", //教师名字
            "content" => "require|min:200",
            "oneClass" => "require",
        ];
        $msg = [
            "name.require" => "名称必须",
            "name.max" => "名称最多不能超过25个字符",
            "stu_id.cherkStuId" => "学号不符合格式",
            "stu_id.require" => "学号必须填写",
            "stu_name.require" => "学生名字不能为空",
            "tea_id.require" => "教师工号必填",
            "tea_id.number" => "教师工号格式不对",
            "tea_name.require" => "教师名字必填",
            "content.require" => "内容最少200字",
            "content.min" => "内容最少200字",
            "oneClass.require" => "请选择获奖类别",

        ];
        $validate = new Validate($rule, $msg);
        $validate->extend([
            'cherkStuId' => function ($value) {
                $preg = "/^[0-9]{2}50[0-9]{5}$/";
                if (preg_match($preg, $value) == 0) {
                    return false;
                } else {
                    return true;
                }
            },
        ]);

        $result = $validate->check($data);
        if (!$result) {
            return $validate->getError();
        }
        $haveS = $this->haveStu($stu_id, $stu_name);
        if (!$haveS) {
            return "学号或姓名错误";
        }
        $haveT = $this->haveTea($tea_id, $tea_name);
        if (!$haveT) {
            return "教师工号或姓名错误";
        }
        $classId = Db::table($type)->where('class', $oneClass)->where('grade', $twoClass)->find();

        $data = array(
            "type" => $type,
            "name" => $name,
            "time" => date('y-m-d h:i:s'),
            "stu_id" => $stu_id,
            "tea_id" => $tea_id,
            "content" => $content,
            "class_id" => $classId["id"],
            "score" => $classId["score"],
            "is_access" => 0,
        );
        $res = Db::table('score_manage')->insert($data);
        return $res;

        //$data = $_POST;
        //$data['time'] = date('y-m-d h:i:s');
        // $res = Db::table('project')->insert($data);
        // return $res;
    }

    //验证学生
    public function haveStu($stu_id, $name)
    {
        $res = Db::table('user')->where('stu_id', $stu_id)->where('name', $name)->find();
        if ($res == "") {
            return 0;
        } else {
            return 1;
        }
        // return $res;
    }
    //验证老师
    public function haveTea($tea_id, $name)
    {
        $res = Db::table('teacher')->where('tea_id', $tea_id)->where('name', $name)->find();
        if ($res == "") {
            return 0;
        } else {
            return 1;
        }

        // return $res;
    }

    //验证页面
    public function submitMaterial()
    {
        if (getCookie()) {

            $select = Db::table('project')->where('stu_id', getCookie())->select();
        } else {
            $select = array();
        }
        $this->assign('keys', count($select));
        $this->assign('list', $select);
        return $this->fetch('Material');
    }

    //类别
    public function getOneClass($type)
    {
        $select = Db::table($type)->distinct(true)->field('class')->select();
        $select = json_encode($select);
        return $select;
    }

    public function getTwoClass($type, $class)
    {
        $select = Db::table($type)->field('grade,score')->where('class', $class)->select();
        $select = json_encode($select);
        return $select;
    }
    //end 类别

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function appProject()
    {
        return $this->fetch('appProject');
    }

    public function getProList()
    {
        $res = Db::table('p_class')->select();
        return $res;
        # code...
    }
    public function upDoc()
    {
        $file = request()->file('file');
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if ($info) {
            // 成功上传后 获取上传信息
            // 输出 jpg
            //echo $info->getExtension();
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            return json_encode(array('state' => 1, 'message' => $info->getSaveName()));
            // 输出 42a79759f284b767dfcb2a0197904287.jpg
            //echo $info->getFilename();
        } else {
            // 上传失败获取错误信息
            return json_encode(array('state' => 0, 'message' => $file->getError()));
        }

        return dump($file);
        # code...
    }

    public function appProInfo($pro_name, $pro_type, $pro_school, $pro_dept, $money, $stu_name, $stu_id, $stu_number, $stu_phone, $stu_credit, $tea_id, $tea_name, $describe, $materials, $class_id, $score)
    {
        $haveS = $this->haveStu($stu_id, $stu_name);
        if (!$haveS) {
            return json_encode(['status' => 0, 'message' => "学号或姓名错误"]);
        }
        $haveT = $this->haveTea($tea_id, $tea_name);
        if ($haveT) {
            return json_encode(['status' => 0, 'message' => "教师工号或姓名错误"]);
        }
        $data = [
            'time' => date('y-m-d h:i:s'),
            'name' => $pro_name,
            'tea_id' => $tea_id,
            'stu_id' => $stu_id,
            'content' => $describe,
            'is_access'=>0,
            'number' => $stu_number,
            'class_id' => $class_id,
            'school' => $pro_school,
            'dept' => $pro_dept,
            'score' => $score,
            'type' => $pro_type,
            'phone' => $stu_phone,
            'credit_card' => $stu_credit,
            'materials' => $materials,

        ];
        Db::startTrans();
        try {
            if (Db::table('project_manage')->insert($data) && Db::table('p_member')->insert([
                's_apply_id' => Db::name('project_manage')->getLastInsID(),
                'stu_id' => $stu_id,
                'stu_name' => $stu_name,
                'phone' => $stu_phone,
                'status' => '1',
            ]))
// 提交事务
            {
                Db::commit();
                return json_encode(['status' => 1, 'message' => '申请成功']);
            } else {
                Db::rollback();
                return json_encode(['status' => 0, 'message' => '申请失败']);
            }

        } catch (\Exception $e) {
// 回滚事务
            Db::rollback();
            return json_encode(['status' => 0, 'message' => '申请失败']);
        }
        # code...
    }

    public function addProMember()
    {
        return $this->fetch('addProMember');

        # code...
    }

    public function getProjectList($class_id)
    {
        $res = Db::table('project_manage')->where('is_access', 1)->where('class_id', $class_id)->select();
        for ($i = 0; $i < count($res); $i = $i + 1) {
            $num = Db::table('p_member')->where('s_apply_id', $res[$i]['s_apply_id'])->where('status!=3')->count();
            $res[$i]['NowNum'] = $num;
        }

        return $res;
        # code...
    }

    public function addNumber($s_apply_id, $stu_id, $stu_name, $stu_phone, $stu_credit)
    {
        $haveS = $this->haveStu($stu_id, $stu_name);
        if (!$haveS) {
            return "学号或姓名错误";
        }
        $data = [
            's_apply_id' => $s_apply_id,
            'stu_id' => $stu_id,
            'stu_name' => $stu_name,
            'phone' => $stu_phone,
            'stu_credit' => $stu_credit,
            'status' => '3'];
        $res = Db::table('p_member')->insert($data);
        if ($res = 1) {
            $ret = [
                'static' => 1,
                'message' => "插入成功",
            ];
        } else {
            $ret = [
                'static' => 1,
                'message' => "插入成功",
            ];
        }
        return json_encode($ret);
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    public function getMsg()
    {
        $res = Db::table('p_member')->field('s_apply_id')->where(['stu_id' => getCookie(), 'status' => 1])->select();
        $res = array_column($res, 's_apply_id');
        $sum = Db::table('p_member')->where('status', 3)->where('s_apply_id', 'in', $res)->count();
        return $sum;
        # code...
    }

    public function appAccess(Type $var = null)
    {
        # code...
    }

}
