<?php
namespace app\index\controller;

use think\Controller;
use think\Cookie;

class CommonController extends Controller
{
    public function _initialize()
    {
        $uid = Cookie::has('stu_id');
        if (!$uid) {
            $this->redirect('index/sign/sign', '请先登录后操作');
        }
        $this->assign('stu_id', getUserName());
    }

}
