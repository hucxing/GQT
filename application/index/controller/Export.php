<?php
namespace app\index\controller;
use think\Db;

class Export extends CommonController
{
    public function export()
    {
        return $this->fetch('export');
    }

    //个人信息
    public function get_person(){
        $res = Db::table('user')->where('stu_id',getCookie())->select();
        return json($res);
    }
    //德育
    public function get_varture(){
        $res = Db::table('score_manage')->where('stu_id',getCookie())->where('type','varture')->select();
        return json($res);
    }
    //智育
    public function get_wit(){
        $res = Db::table('score_manage')->where('stu_id',getCookie())->where('type','wit')->select();
        return json($res);
    }
    //体育
    public function get_sport(){
        $res = Db::table('score_manage')->where('stu_id',getCookie())->where('type','sports')->select();
        return json($res);
    }
    public function get_other(){
        $res = Db::table('score_manage')->where('stu_id',getCookie())->where('type','other_add')->select();
        return json($res);
    }
}
