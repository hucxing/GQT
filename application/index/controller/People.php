<?php
namespace app\index\controller;

use think\Db;

class People extends CommonController
{
    public function index()
    {

        // $this->checkSignEd();
        return $this->fetch();
    }

    public function getMessage()
    {
        $res = Db::table('p_member')->field('s_apply_id')->where(['stu_id' => getCookie(), 'status' => 1])->select();
        $res = array_column($res, 's_apply_id');
        $sum = Db::table('p_member')->alias('m')->join('project_manage p ', 'm.s_apply_id= p.s_apply_id')->where('m.status', 3)->where('m.s_apply_id', 'in', $res)->select();
        return $sum;
    }

    public function allow($id)
    {
        $res = Db::table('p_member')->where('id', $id)->update(['status' => "2"]);
        return $res;

    }
    public function repulse($id)
    {
        $res = Db::table('p_member')->where('id', $id)->update(['status' => "4"]);
        return $res;
        # code...
    }

    public function getScore()
    {
        $res = Db::table('score_manage')->where('stu_id', getCookie())->order('s_apply_id')->select();
        for ($i = 0; $i < count($res); $i++) {
            $res[$i]['ZhType'] = Db::table($res[$i]['type'])->where('id', $res[$i]['class_id'])->find();
            if ($res[$i]['type'] == "varture") {
                $res[$i]['type'] = "德育";
            } else if ($res[$i]['type' == "wit"]) {
                $res[$i]['type'] = "智育";
            } else if ($res[$i]['type'] == "sports") {
                $res[$i]['type'] = "体育";
            } else if ($res[$i]['type'] == "other_add") {
                $res[$i]['type'] = "其他";
            } else if ($res[$i]['type']) {
                $res[$i]['type'] = "其他";
            }

        }
        return $res;
        # code...
    }

    public function getPeoPro()
    {
        $feild = [
            'c.name' => 'class_name',
            'p.name',
            'p.dept',
            'm.stu_name',
            'status',
            'is_access',
            'p.score',

        ];

        $sum = Db::table('p_member')->alias('m')->join('project_manage p ', 'm.s_apply_id= p.s_apply_id')->join('p_class c', 'p.class_id=c.id')->where('m.stu_id', getCookie())->where('m.status', 'in', [1, 2])->where('p.is_access', 'in', [0, 1, 2])->field($feild)->select();

        return $sum;
        # code...
    }

    public function inster($class, $num)
    {

        $res = Db::table('user')->where('stu_id', getCookie())->update([$class => $num]);
    }

    public function insterZ($virture, $sport, $wit, $other_score)
    {

        $res = Db::table('user')->where('stu_id', getCookie())->update(['virture' => $virture, 'sport' => $sport, 'wit' => $wit, 'other_score' => $other_score]);
    }

}
