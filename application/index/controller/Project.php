<?php
namespace app\index\controller;
use think\Db;

class Project extends CommonController
{
    public function project()
    {
        return $this->fetch('project');
    }
    //发布项目信息
    public function get_tea_data(){
        $sql = "select p.name as p_name,p.number as num,p.id as id,t.name as t_name,p.time as time,p.end_time as e_time,p.score as score from p_class as p ,teacher as t where p.tea_id=t.tea_id";
        $res = Db::query($sql);
        return json($res);
    }
    //申请进度信息
    public function get_score_data($stu_id){
        $res = Db::table('score_manage')->where('stu_id',$stu_id)->select();
        // var_dump(json($res));
        return json($res);
    }
    public function get_pro_data($stu_id){
        $data = (int)$stu_id;
        $sql = "select project_manage.name as p_name,project_manage.score as score,project_manage.time as time,project_manage.is_access AS is_access,p_class.name AS pc_name from project_manage , p_class WHERE project_manage.class_id = p_class.id and project_manage.stu_id = $data";
        $res = Db::query($sql);
        // var_dump(json($res));
        return json($res);
    }

}