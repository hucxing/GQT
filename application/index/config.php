<?php
return [
    'root' => '/GQT/public',
    'view_replace_str' => [
        '$image' => '/GQT/public/static/image',
        '$bootstrap/css' => '/GQT/public/static/bootstrap/css/bootstrap.min.css',
        '$bootstrap/js' => '/GQT/public/static/bootstrap/js/bootstrap.min.js',
        '$vue' => '/GQT/public/static/vue/vue.js',
        '$jquery' => '/GQT/public/static/jquery/jquery-3.3.1.js',
        '$fileinput.css' => '/GQT/public/static/jquery/fileinput.css',
        '$fileinput.js' => '/GQT/public/static/jquery/fileinput.js',
        '$js' => '/GQT/public/static/js',
        '$css' => '/GQT/public/static/css',
        '$gqt' => '/GQT/public/static/',
    ],
];
