<?php
use think\Db;
use think\Cookie;
/**
 * @Author: Marte
 * @Date:   2018-04-11 21:37:35
 * @Last Modified by:   Marte
 * @Last Modified time: 2018-04-12 20:25:43
 */
    function route($function)
    {
        if ($function!=null) {
        
        $data=Db::table('function')->field('route')->where('function',$function)->select();
        // var_dump($data);
        return $data[0]['route'];
        }
        else
        {
            return null;
        }
    }
    function Module($function)
    {
        // var_dump($function);
        $data=Db::table('founction_module')->field('module')->where('name',$function)->select();
        // var_dump($data);
        return $data[0]['module'];

    }
    //验证是否登陆
    function checkSign()
    {
        if (Cookie::has('tea_id')) {
            return;
        } else {
            return redirect('/GQT/public/admin/index/sign',200);
        }

    }

    function getCookie()
    {
        return Cookie::get('tea_id');
    }

    function getUserName()
    {
        $tea = Db::table('teacher')->field(['name'])->where('tea_id', getCookie())->select();
        return $tea[0]['name'];
    }