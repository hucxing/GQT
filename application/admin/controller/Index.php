<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use \Page;
use think\Cookie;

class Index extends CommController
{
    public function index()
    {
        // var_dump(getCookie());
        $data=Db::table('teacher')->field('score_apply,score_pass,pro_apply,pro_pass,pro_success,pro_build,teacher_info,user_info,manage_add,score_query,pro_query,announce,operation,excel_export,excel_import')->where('tea_id',getCookie())->select();
        $route=Db::table('function')->select();
        $score_apply=$data[0]['score_apply'];
        $score_apply_r=route($score_apply);
        $score_pass=$data[0]['score_pass'];
        $score_pass_r=route($score_pass);
        if($score_apply!=null||$score_pass!=null)
        {

            $score_manage=Module("score_manage");
            //return var_dump($score_manage);
        }
        else
        {
            $score_manage=null;
        }

        $pro_apply=$data[0]['pro_apply'];
        $pro_apply_r=route($pro_apply);

        $pro_pass=$data[0]['pro_pass'];
        $pro_pass_r=route($pro_pass);

        $pro_success=$data[0]['pro_success'];
        $pro_success_r=route($pro_success);

        $pro_build=$data[0]['pro_build'];
        $pro_build_r=route($pro_build);
        if($pro_apply!=null||$pro_pass!=null||$pro_success!=null||$pro_build!=null)
        {

            $pro_manage=Module("pro_manage");
        }
        else
        {
            $pro_manage=null;
        }


        $teacher_info=$data[0]['teacher_info'];
        $teacher_info_r=route($teacher_info);
        $user_info=$data[0]['user_info'];
        $user_info_r=route($user_info);
        $manage_add=$data[0]['manage_add'];
        $manage_add_r=route($manage_add);
        if($teacher_info!=null||$user_info!=null||$manage_add!=null)
        {

            $user_manage=Module("user_manage");
        }
        else
        {
            $user_manage=null;
        }

        $score_query=$data[0]['score_query'];
        $score_query_r=route($score_query);
        // $pro_query=$data[0]['pro_query'];
        // $pro_query_r=route($pro_query);
         if($score_query!=null)
        {

            $statistical_query=Module("statistical_query");
        }
        else
        {
            $statistical_query=null;
        }

        $announce=$data[0]['announce'];
        $announce_r=route($announce);
        $operation=$data[0]['operation'];
        $operation_r=route($operation);
        if($announce!=null||$operation!=null)
        {

            $info_manage=Module("info_manage");
        }
        else
        {
            $info_manage=null;
        }
        $excel_import=$data[0]['excel_import'];
        $excel_import_r=route($excel_import);
        $excel_export=$data[0]['excel_export'];
        $excel_export_r=route($excel_export);
        if($excel_import!=null||$excel_export!=null)
        {

            $imp_exp=Module("statistical_query");
        }
        else
        {
            $imp_exp=null;
        }
        $this->assign('name',getUserName());
        $this->assign(['imp_exp'=>$imp_exp,'info_manage'=>$info_manage,'statistical_query'=>$statistical_query,'user_manage'=>$user_manage,'pro_manage'=>$pro_manage,'score_manage'=>$score_manage]);
        $this->assign(['score_apply_r'=>$score_apply_r,'score_pass_r'=>$score_pass_r,'pro_apply_r'=>$pro_apply_r,'pro_pass_r'=>$pro_pass_r,'pro_success_r'=>$pro_success_r,'pro_build_r'=>$pro_build_r,'teacher_info_r'=>$teacher_info_r,'user_info_r'=>$user_info_r,'manage_add_r'=>$manage_add_r,'score_query_r'=>$score_query_r,'announce_r'=>$announce_r,'operation_r'=>$operation_r,'excel_import_r'=>$excel_import_r,'excel_export_r'=>$excel_export_r]);
        $this->assign(['score_apply'=>$score_apply,'score_pass'=>$score_pass,'pro_apply'=>$pro_apply,'pro_pass'=>$pro_pass,'pro_success'=>$pro_success,'pro_build'=>$pro_build,'teacher_info'=>$teacher_info,'user_info'=>$user_info,'manage_add'=>$manage_add,'score_query'=>$score_query,'announce'=>$announce,'operation'=>$operation,'excel_export'=>$excel_export,'excel_import'=>$excel_import]);
        return $this->fetch();
    }
    
    //insert
    public function insert(){
        $db = Db::table('user');
        $stu_id=input('stu_id');
        $pass=input('password');
        $name=input('name');
        $class=input('class');
        $sex=input('sex');
        $grade=input('grade');
        $istrue=Db::table('user')->where('stu_id',$stu_id)->select();
        if ($istrue) {
           $this->error('添加失败！该学号已存在！','usermanage/stu_info');
        }
        else
        {
            $insert=$db->insert(['stu_id'=>$stu_id,'password'=>$pass,'name'=>$name,'class'=>$class,'sex'=>$sex,'grade'=>$grade]);
            if($insert){
                $this->success('添加成功!','usermanage/stu_info');
            }else{
                $this->error('添加错误!','usermanage/stu_info');
            }
        }
    }
    //update
    public function update($id){
        $db = Db::table('user');
        $stu_id=input('stu_id');
        // var_dump($stu_id);
        $pass=input('password');
        $name=input('name');
        $class=input('class');
        $sex=input('sex');
        $grade=input('grade');
        // $score=input('score');
        $insert=$db->where(['stu_id'=>$id])->update(['stu_id'=>$stu_id,'password'=>$pass,'name'=>$name,'class'=>$class,'sex'=>$sex,'grade'=>$grade]);
        var_dump($insert);
        if($insert){
            $this->success('添加成功!','usermanage/stu_info');
        }else{
            $this->error('添加错误!','usermanage/stu_info');
        }
    }
    //delete
    public function delete($id){
         $db = Db::table('user');
         $delete=$db->where('stu_id',$id)->delete();
        if($delete){
            $this->success('删除成功!','usermanage/stu_info');
        }else{
            $this->error('删除失败!','usermanage/stu_info');
        }
    }
   public function basic_info(){

        //var_dump(getCookie());
       $tea_id=Db::table('teacher')->where('tea_id', getCookie())->select();
       $this->assign('tea_id',$tea_id);
       // var_dump($tea_id);
        return $this->fetch('basic_info');
    }

    public function update_underinfo(){
        $db = Db::table('teacher');
        $tea_id=input('tea_id');
        $name = input('name');
        $password = input('password');
        $title=input('title');
        $sex=input('sex');
        $dupt=input('dupt');
        $dept=input('dept');
        $updata=$db->where('tea_id',$tea_id)->update(['tea_id'=>$tea_id,'name'=>$name,'password'=>$password,'sex'=>$sex,'dupt'=>$dupt,'title'=>$title,'dept'=>$dept]);
        return $updata;
    }




}