<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use TCPDF;
use think\Loader;
class Pdf extends CommController
{
    public function pdf_exp(){
      return $this->fetch('imp_exp/pdf_exp');
    }
    public function export_pdf()
    {
        Loader::import('tcpdf.tcpdf');
        /*新建一个pdf文件：
       Orientation：orientation属性用来设置文档打印格式是“Portrait”还是“Landscape”。 Landscape为横式打印，Portrait为纵向打印
       Unit：设置页面的单位。pt：点为单位，mm：毫米为单位，cm：厘米为单位，in：英尺为单位
       Format：设置打印格式，一般设置为A4
       Unicode：为true，输入的文本为Unicode字符文本
       Encoding：设置编码格式，默认为utf-8
       Diskcache：为true，通过使用文件系统的临时缓存数据减少RAM的内存使用。 */
        // $pdf = new \tcpdf('A4-L');
        //去掉页眉页脚
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetFont('stsongstdlight', '', 12);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $title = <<<EOD
        <h2>HTML TABLE:</h2>
                <table>
                    <tr>
                        <td>姓名：</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>民族：</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>学号：</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>系别：</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>专业：</td>
                        <td></td>
                    </tr>
                    <tr>
                            <td>获得学分:</td>
                            <td></td>
                        </tr>
                    <tr>
                        <td>证件号:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>入学年月:</td>
                        <td></td>
                    </tr>
                </table>
EOD;
        $pdf->writeHTML($title, true, false, true, false, '');
        $pdf->Output('1.pdf', 'I');
        // $pdf->Output('1.pdf', 'D');
        // 在"D"输出方式下，下载下来的1.pdf文件能正常打开并显示 

    }
} 
