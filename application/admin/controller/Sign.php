<?php
namespace app\admin\controller;

use think\Controller;
use think\Cookie;
use think\Db;

class Sign extends Controller
{

    public function sign()
    {
        return $this->fetch('sign');
    }

    public function verifyId($user)
    {
        $res = Db::table('teacher')->where('tea_id', $user)->find();
        if (is_null($res)) {
            $arr = array(
                'succ' => false,
                'state' => 0,
                'data' => '',
            );
        } else {
            $arr = array(
                'succ' => true,
                'state' => 1,
                'data' => '',
            );
        }
        $res = json_encode($arr);
        return $res;
    }

    public function submit($user, $password)
    {

        $res = Db::table('teacher')->where('tea_id', $user)->value('password');
        if ($res == $password) {
            $arr = array(
                'succ' => true,
                'state' => 1,
                'data' => '',
            );
            Cookie::set('tea_id', $user, time() + 3600);
            //           Cookie::set('password',$password,time()+3600);
        } else {
            $arr = array(
                'succ' => false,
                'state' => 0,
                'data' => '',
            );
        }
        $res = json_encode($arr);
        return $res;
    }

    public function logout()
    {
        Cookie::delete('tea_id');
        return $this->fetch('sign');
    }

}
