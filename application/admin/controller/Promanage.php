<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use \Page;
class Promanage extends CommController{

    public  function pro_apply()
    {   
        $data=Db::table('project_manage')->where('is_access',0)->order('s_apply_id asc')->select();
        // var_dump($data);
        $count=db('project_manage')->where('is_access',0)->count('s_apply_id');
         $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('pro_manage/pro_apply');
    } 
    public  function pro_pass()
    {   
        $data=Db::table('project_manage')->where('is_access',1)->order('s_apply_id asc')->select();
        $count=db('project_manage')->where('is_access',1)->count('s_apply_id');
         $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('pro_manage/pro_pass');
    } 
    public  function pro_success()
    {   
        $data=Db::table('project_manage')->where('is_access',2)->order('s_apply_id asc')->select();
        $count=db('project_manage')->where('is_access',2)->count('s_apply_id');
         $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('pro_manage/pro_success');
    } 
    public function pass($id)
    {
        $update=db('project_manage')->where('s_apply_id',$id)->update(['is_access'=>1]);
        if ($update) {
            $this->success('通过成功!','promanage/pro_apply');
        }
        else{
            $this->error('修改失败!','promanage/pro_apply');
        }
    }
    public function unpass($id)
    {
        $update=db('project_manage')->where('s_apply_id',$id)->update(['is_access'=>-1]);
        if ($update) {
            $this->success('修改成功!','promanage/pro_apply');
        }
        else{
            $this->error('修改失败!','promanage/pro_apply');
        }
    }
    public function pass1($id)
    {
        $update=db('project_manage')->where('s_apply_id',$id)->update(['is_access'=>2]);
        if ($update) {
            $this->success('通过成功!','promanage/pro_pass');
        }
        else{
            $this->error('修改失败!','promanage/pro_pass');
        }
    }
    public function unpass1($id)
    {
        $update=db('project_manage')->where('s_apply_id',$id)->update(['is_access'=>-1]);
        if ($update) {
            $this->success('修改成功!','promanage/pro_pass');
        }
        else{
            $this->error('修改失败!','promanage/pro_pass');
        }
    }

    public function pro_build()
    {
        return $this->fetch('pro_manage/pro_build');
    }
    public function build_success()
    {
        $name=input('name');
        $numbers=input('numbers');
        $max_money=input('max_money');
        $score=input('score');
        $start=input('start');
        $end=input('end');
        $add_name=input('add_name');
        $school=input('school');
        $teacher=input('teacher');
        $dept=input('dept');
        $description=input('description');
        $material=input('material');
        $tea_id="123";
        $title=input('title');
        // var_dump($start);
        $data=Db::table('p_class')->insert(['name'=>$name,'tea_id'=>$tea_id,'number'=>$numbers,'max_money'=>$max_money,'score'=>$score,'time'=>$start,'end_time'=>$end,'add_name'=>$add_name,'school'=>$school,'adviser'=>$teacher,'dept'=>$dept,'describe'=>$description,'materials'=>$material,'tea_id'=>$tea_id,'title'=>$title]);
        // var_dump($data);
    }
}