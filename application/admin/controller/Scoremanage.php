<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use \Page;

class Scoremanage extends CommController
{
    //项目申请
    public function score_apply(){
        $data=Db::table('score_manage')->where('is_access',0)->order('s_apply_id asc')->select();
        // var_dump($data);
        $count=db('score_manage')->where('is_access',0)->count('s_apply_id');
         $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('score_manage/score_apply');
    }
    public function pass($id)
    {
        $update=db('score_manage')->where('s_apply_id',$id)->update(['is_access'=>1]);
        if ($update) {
            $this->success('通过成功!','Scoremanage/score_apply');
        }
        else{
            $this->error('修改失败!','Scoremanage/score_apply');
        }
    }
     //学分申请通过 progres=1
    public function score_pass(){
        // $db = Db::table('score_manage');
        $data=Db::table('score_manage')->where('is_access',1)->order('s_apply_id asc')->select();
        $count=db('score_manage')->where('is_access',1)->count('s_apply_id');
         $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('score_manage/score_pass');
    }
    
}