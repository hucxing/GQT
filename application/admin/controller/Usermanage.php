<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use \Page;
use think\Request; 
/**
* 
*/
class Usermanage extends CommController
{
    
    //学生信息
    public function stu_info(){
        $data=Db::table('user')->order('stu_id asc')->select();
        $count=db('user')->count('stu_id');
        $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        // var_dump($data);
        return $this->fetch('user_manage/stu_info');
    }
    //教师信息
    public function teacher_info(){
        $data=Db::table('teacher')->order('tea_id asc')->select();
        $count=db('teacher')->count('tea_id');
        $this->assign('count',$count);
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('user_manage/teacher_info');
    }
    public function delete($id){
         $db = Db::table('teacher');
         $delete=$db->where('tea_id',$id)->delete();
        if($delete){
            $this->success('删除成功!','usermanage/teacher_info');
        }else{
            $this->error('删除失败!','usermanage/teacher_info');
        }
    }
    //管理员添加
    public function manage_add(){
     return  $this->fetch('user_manage/manage_add');
    }
    public function manage_insert(){
        $a=Request::instance();
        $tea_id=$a->post('tea_id');
        $data=Db::table('teacher')->where('tea_id',$tea_id)->select();
        // var_dump($data);
        if ($data) 
        {
            return false;
        }
        else
        {
            $dept=$a->post('dept');
            $dupt=$a->post('dupt');
            $title=$a->post('title');
            $password=$a->post('password');
            $sex=$a->post('sex');
            $name=$a->post('name');

            $score_apply=$a->post('score_apply');
            $score_pass=$a->post('score_pass');

            $pro_apply=$a->post('pro_apply');
            $pro_pass=$a->post('pro_pass');
            $success=$a->post('success');
            $pro_build=$a->post('pro_build');

            $user_info=$a->post('user_info');
            $teacher_info=$a->post('teacher_info');
            $manage_add=$a->post('manage_add');

            $announce=$a->post('announce');
            $operation=$a->post('operation');

            // $excel_export=$a->post('excel_export');
            // $excel_import=$a->post('excel_import');

            $score_query=$a->post('score_query');
            $pro_query=$a->post('pro_query');

            $insert=Db::table('teacher')->insert(['title'=>$title,'dupt'=>$dupt,'dept'=>$dept,'tea_id'=>$tea_id,'name'=>$name,'password'=>$password,'sex'=>$sex,'score_apply'=>$score_apply,'score_pass'=>$score_pass,'pro_apply'=>$pro_apply,'pro_pass'=>$pro_pass,'pro_success'=>$success,'pro_build'=>$pro_build,'user_info'=>$user_info,'teacher_info'=>$teacher_info,'manage_add'=>$manage_add,'announce'=>$announce,'operation'=>$operation,'score_query'=>$score_query,'pro_query'=>$pro_query]);
            if ($insert) {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
// tea_id: 165042119
// password: 123456
// sex: 男
// score_apply: 学分申请
// score_pass: 通过信息
// pro_success: 
// pro_apply: 项目申请
// pro_pass: 项目通过
// pro_build: 项目创建
// user_info: 学生信息
// teacher_info: 教师信息
// manage_add: 管理员添加
// announce: 消息发布
// operation: 消息操作
// excel_export: 信息导出
// excel_import: 信息导入
// score_query: 学分查询
// pro_query: 项目查询
// manage_insert   manage_insert   