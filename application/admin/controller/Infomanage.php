<?php
namespace app\admin\controller;
use \think\Controller;
use think\Db;
use \Page;
use think\Request; 
class Infomanage extends CommController
{
     //消息发布
    public function announce(){
        return $this->fetch('info_manage/announce');
    }
    //消息发布insert
    public function news_insert()
    {
        $db = Db::table('page');
        $page_title=input('title');
        if(request()->isPost()){
            
            $page_content = input('post.editor');
            $content= strip_tags($page_content);
            // var_dump($conters);
            
        }
        $time = date("Y-m-d H:i:s");
        $user_id=getCookie();//管理员身份
        //正则得到图片的路径
        $pattern1= '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
        $m1=$m2=array();
            
        preg_match_all($pattern1,$page_content,$m1);
        //多个图片路径拼接成一个字符用 ，隔开
        //去掉末尾多余的
        if (preg_match_all($pattern1,$page_content,$m1)!=0) {
        $imges=$m1[1][0];
        }
        else
        {
          $imges=null;
        }
        $insert=$db->insert(['page_title'=>$page_title,'page_content'=>$page_content,'page_time'=>$time,'user_id'=>$user_id,'page_image'=>$imges,'content'=>$content]);
        if($insert){
            $this->success('添加成功!','infomanage/announce');
        }else{
            $this->error('添加错误!','infomanage/announce');
        }
    }
    //消息操作
       public function operation()
       {
         $data=db('page')->select();
        $p = new Page($data,20);
        $this->assign('list',$p);
        return $this->fetch('info_manage/operation');
       }
    //消息修改
    public function chang_announce($id)
    {
        $data=db('page')->where('page_id',$id)->select();
        $this->assign('data',$data[0]);
        // var_dump($data['0']['page_title']);
      return  $this->fetch('info_manage/change_announce');
    }
    public function news_update($id)
    {   
        // var_dump($id);
        $db = Db::table('page');
        $page_title=input('title');
        if(request()->isPost()){
            
            $page_content = input('post.editor');
            $content= strip_tags($page_content);
            // var_dump($conters);
            
        }
        $time = date("Y-m-d H:i:s");
        // $user_id=getCookie();//管理员身份
        //正则得到图片的路径
        $pattern1= '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
        $m1=$m2=array();
            
        preg_match_all($pattern1,$page_content,$m1);
        //多个图片路径拼接成一个字符用 ，隔开
        //去掉末尾多余的
        if (preg_match_all($pattern1,$page_content,$m1)!=0) {
        $imges=$m1[1][0];
        }
        else
        {
          $imges=null;
        }
        $insert=$db->where('page_id',$id)->update(['page_title'=>$page_title,'page_content'=>$page_content,'page_time'=>$time,'page_image'=>$imges,'content'=>$content]);
        if($insert){
            $this->success('添加成功!','infomanage/announce');
        }else{
            $this->error('添加错误!','infomanage/announce');
        } 
        return aaa;
    }
}