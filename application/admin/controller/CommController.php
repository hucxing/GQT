<?php
namespace app\admin\controller;

use think\Controller;
use think\Cookie;

class CommController extends Controller
{
    public function _initialize()
    {
        $tea_id = Cookie::has('tea_id');
        if (!$tea_id) {
            $this->redirect('admin/sign/sign', '请先登录后操作');
        }
        $this->assign('tea_id', getUserName());
    }

}