$(document).ready(function() {
    $('#user-name').blur(function() {
        $.get('/GQT/public/index/Sign/verifyId', {
            user: this.value
        }, function(data) {
            let res = JSON.parse(data)
            if (res.succ) {
                $('#user-div').removeClass('has-error').addClass('has-success')
                $('#user-div .input-Prompt').removeClass('glyphicon-remove').addClass('glyphicon-ok')
            } else {
                $('#user-div').removeClass('has-success').addClass('has-error')
                $('#user-div .input-Prompt').removeClass('glyphicon-ok').addClass('glyphicon-remove')
            }
        })
    })

    $('form').submit(function() {
            $.post('/GQT/public/admin/Sign/submit', {
                    user: $('#user-name')[0].value,
                    password: $('#password')[0].value
                },
                function(data) {
                    let res = JSON.parse(data)
                    if (res.succ) {
                        window.location.href = '/GQT/public/admin';
                    } else {
                        $('form').shake(5, 20, 100)

                    }
                })

            return false;
        })
        //给jq增加抖动效果
    jQuery.fn.shake = function(intShakes /*Amount of shakes*/ , intDistance /*Shake distance*/ , intDuration /*Time duration*/ ) {
        this.each(function() {
            let jqNode = $(this)
            while (intShakes--) {
                jqNode.animate({ //相对左边运动，right没用
                    left: intDistance * -1
                }, intDuration / 4).animate({
                    left: intDistance
                }, intDuration / 2).animate({
                    left: 0
                }, intDistance / 4)
            }
        })
    }


})